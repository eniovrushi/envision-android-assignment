package com.eniovrushi.assignment

import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import java.io.File

class OpenFileActivity : AppCompatActivity() {
    private lateinit var textView : TextView
    private lateinit var delete : Button
    private lateinit var back : Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_open_file)
        back = findViewById(R.id.back)
        back.setOnClickListener {
            finish()
        }
        textView = findViewById(R.id.textView)
        val filename = intent.getStringExtra("filename")
        val file = File(this.filesDir, filename!!)
        textView.text = file.readText()
        delete = findViewById(R.id.delete)
        delete.setOnClickListener {
            file.delete()
            finish()
        }
    }
}