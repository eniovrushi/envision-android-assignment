package com.eniovrushi.assignment

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.FrameLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.camera.core.*
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.core.os.postDelayed
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.tabs.TabLayout
import com.google.common.util.concurrent.ListenableFuture
import com.google.gson.Gson
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.Response
import java.io.File
import java.io.IOException
import java.lang.Exception
import java.util.*
import java.util.concurrent.Executors
import kotlin.text.StringBuilder

class MainActivity : AppCompatActivity() {

    private val client : OkHttpClient = OkHttpClient()

    private val gson = Gson()

    private var isCameraSetUp : Boolean = false
    private var cameraProvider : ProcessCameraProvider? = null
    private var preview : Preview? = null
    private var cameraSelector : CameraSelector? = null
    private var camera : Camera? = null

    private lateinit var popup : CardView
    private lateinit var capture : Button
    private lateinit var message : TextView
    private lateinit var tabLayout : TabLayout
    private lateinit var container : FrameLayout
    private lateinit var previewView : PreviewView
    private lateinit var recycleView : RecyclerView
    private lateinit var cameraProviderFuture : ListenableFuture<ProcessCameraProvider>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recycleView = findViewById(R.id.recycleView)
        tabLayout = findViewById(R.id.tabLayout)
        container = findViewById(R.id.container)
        previewView = findViewById(R.id.previewView)
        popup = findViewById(R.id.popup)
        message = findViewById(R.id.message)
        capture = findViewById(R.id.capture)
        popup.visibility = View.GONE

        val imageCapture = ImageCapture.Builder()
            .build()

        recycleView.layoutManager = LinearLayoutManager(applicationContext, LinearLayoutManager.VERTICAL, false)
        recycleView.adapter = ListAdapter(this.fileList(), this)
        val adapter = recycleView.adapter
        adapter?.notifyDataSetChanged()

        capture.setOnClickListener {
            popup.visibility = View.VISIBLE
            message.text = getString(R.string.please_wait)
            message.announceForAccessibility(getString(R.string.please_wait))
            cameraProvider?.unbind(preview)
            if (isCameraSetUp) {
                isCameraSetUp = false
                val imageFile = File.createTempFile(GregorianCalendar().time.toString(), ".jpg", cacheDir)
                val outputFileOptions = ImageCapture.OutputFileOptions.Builder(imageFile).build()
                imageCapture.takePicture(
                    outputFileOptions,
                    Executors.newSingleThreadExecutor(),
                    object : ImageCapture.OnImageSavedCallback {
                        override fun onImageSaved(outputFileResults: ImageCapture.OutputFileResults) {
                            val body = MultipartBody.Builder()
                                .setType(MultipartBody.FORM)
                                .addFormDataPart(
                                    "photo",
                                    "idk",
                                    imageFile.asRequestBody("application/octet-stream".toMediaTypeOrNull())
                                )
                                .build()
                            val request = Request.Builder()
                                .url("https://letsenvision.app/api/test/readDocument")
                                .method("POST", body)
                                .addHeader(
                                    "Cookie",
                                    "__cfduid=d97604b6c67574ccd048c013ffbee703a1614774197"
                                )
                                .build()
                            client.newCall(request).enqueue(object : Callback {
                                override fun onFailure(call: Call, e: IOException) {
                                    Log.e("Intelnet", "onFailure: ", e)
                                    Handler(Looper.getMainLooper()).postDelayed(1000) {
                                        popup.visibility = View.GONE
                                    }
                                    imageFile.delete()
                                    Handler(Looper.getMainLooper()).post {
                                        camera = cameraProvider!!.bindToLifecycle(
                                            this@MainActivity,
                                            cameraSelector!!,
                                            imageCapture,
                                            preview
                                        )
                                        isCameraSetUp = true
                                        message.text = getString(R.string.an_error_occurred)
                                        message.announceForAccessibility(getString(R.string.an_error_occurred))
                                    }
                                }

                                override fun onResponse(call: Call, response: Response) {
                                    val serverResponse = response.body!!.string()
                                    Log.d("Intelnet", serverResponse)
                                    try {
                                        val parsedResponse =
                                            gson.fromJson(serverResponse, ResponsePayload::class.java)
                                        if (parsedResponse.response != null) {
                                            val stringBuilder = StringBuilder()
                                            val paragraphs = parsedResponse.response.paragraphs
                                            paragraphs?.forEach {
                                                stringBuilder.append(it.paragraph)
                                                stringBuilder.append(" ")
                                            }
                                            val message = stringBuilder.toString().trim()
                                            val intent =
                                                Intent(this@MainActivity, ReadingActivity::class.java)
                                            intent.putExtra("message", message)
                                            startActivity(intent)
                                        }
                                        imageFile.delete()
                                        Handler(Looper.getMainLooper()).post {
                                            capture.announceForAccessibility("No text found")
                                            camera = cameraProvider!!.bindToLifecycle(
                                                this@MainActivity,
                                                cameraSelector!!,
                                                imageCapture,
                                                preview
                                            )
                                            isCameraSetUp = true
                                            popup.visibility = View.GONE
                                        }
                                    } catch (e : Exception) {
                                        Handler(Looper.getMainLooper()).postDelayed(1000) {
                                            popup.visibility = View.GONE
                                        }
                                        imageFile.delete()
                                        Handler(Looper.getMainLooper()).post {
                                            camera = cameraProvider!!.bindToLifecycle(
                                                this@MainActivity,
                                                cameraSelector!!,
                                                imageCapture,
                                                preview
                                            )
                                            isCameraSetUp = true
                                            message.text = getString(R.string.an_error_occurred)
                                            message.announceForAccessibility(getString(R.string.an_error_occurred))
                                        }
                                    }
                                }
                            })
                        }

                        override fun onError(exception: ImageCaptureException) {
                            message.text = getString(R.string.an_error_occurred)
                            message.announceForAccessibility(getString(R.string.an_error_occurred));
                            Handler(Looper.getMainLooper()).postDelayed(1000) {
                                popup.visibility = View.GONE
                            }
                            camera = cameraProvider!!.bindToLifecycle(
                                this@MainActivity,
                                cameraSelector!!,
                                imageCapture,
                                preview
                            )
                            isCameraSetUp = true
                        }
                    })
            }
        }

        tabLayout.getTabAt(0)?.id = 0
        tabLayout.getTabAt(1)?.id = 1
        tabLayout.selectTab(tabLayout.getTabAt(1))
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                if (tab?.id == 0) {
                    camera = cameraProvider!!.bindToLifecycle(this@MainActivity, cameraSelector!!, imageCapture, preview)
                    isCameraSetUp = true
                    container.visibility = View.VISIBLE
                    capture.visibility = View.VISIBLE
                    tabLayout.announceForAccessibility("Navigate upwards to continue")
                    recycleView.visibility = View.GONE
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                if (tab?.id == 0) {
                    cameraProvider?.unbind(preview)
                    isCameraSetUp = false
                    container.visibility = View.GONE
                    capture.visibility = View.GONE
                    recycleView.visibility = View.VISIBLE
                }
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
                return
            }
        })
        cameraProviderFuture = ProcessCameraProvider.getInstance(this)
        cameraProviderFuture.addListener({
            cameraProvider = cameraProviderFuture.get()
            preview = Preview.Builder()
                .build()
            cameraSelector = CameraSelector.Builder()
                .requireLensFacing(CameraSelector.LENS_FACING_BACK)
                .build()
            preview!!.setSurfaceProvider(previewView.surfaceProvider)
        }, ContextCompat.getMainExecutor(this))
    }

    override fun onResume() {
        super.onResume()
        tabLayout.selectTab(tabLayout.getTabAt(1))
        recycleView.adapter = ListAdapter(this.fileList(), this)
        val adapter = recycleView.adapter
        adapter?.notifyDataSetChanged()
    }
}
