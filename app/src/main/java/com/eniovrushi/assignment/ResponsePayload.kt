package com.eniovrushi.assignment

data class ResponsePayload(
    val response : Response?,
    val message : String?
    )

data class Response(val paragraphs : Array<Paragraph>?)

data class Paragraph(
    val paragraph : String,
    val language : String,
)

