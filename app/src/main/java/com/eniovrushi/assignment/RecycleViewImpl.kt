package com.eniovrushi.assignment

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class ListAdapter(private val dataSet : Array<String>, private val context : Context) : RecyclerView.Adapter<ListAdapter.ViewHolder>() {

    class ViewHolder(view : View) : RecyclerView.ViewHolder(view) {

        val textView: TextView = view.findViewById(R.id.text)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.textView.text = dataSet[position]
        holder.textView.setOnClickListener {
            val intent = Intent(context, OpenFileActivity::class.java)
            intent.putExtra("filename", dataSet[position])
            context.startActivity(intent)
        }
    }

    override fun getItemCount() = dataSet.size
}