package com.eniovrushi.assignment

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.widget.Button
import android.widget.TextView
import java.util.*

class ReadingActivity : AppCompatActivity() {

    private lateinit var textView : TextView
    private lateinit var save : Button
    private lateinit var cancel : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reading)

        textView = findViewById(R.id.textView)
        save = findViewById(R.id.save)
        cancel = findViewById(R.id.cancel)
        textView.movementMethod = ScrollingMovementMethod()
        textView.text = intent.getStringExtra("message")
        save.setOnClickListener {
            this.openFileOutput(GregorianCalendar().time.toString() + ".txt", Context.MODE_PRIVATE).use {
                it.write(textView.text.toString().toByteArray())
                finish()
            }
        }
        cancel.setOnClickListener {
            finish()
        }
    }
}